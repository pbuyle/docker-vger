# Docker Vger

A lean Docker image for the [Vger Gemini server](https://tildegit.org/solene/vger).

This image uses [busybox](https://hub.docker.com/_/busybox) to provide a non-TLS tcp server for Vger.
As a Gemini server the image is incomplete and requires additional server to handle TLS.

## Usage

⚠️ This image requires usage of the `--init` option because it uses a simple shell script which starts two processes.

Simply pass Vger command line parameters as command when running the container.

```
docker run --rm --init registry.gitlab.com/pbuyle/docker-vger -i -l en
```

See the [docker-compose.yml] file for an example using Traefik to take care of TLS.