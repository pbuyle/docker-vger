
#!/bin/sh
set -eu
syslogd -O - -n -l 8 -t &

# Create service user
if ! id vger >/dev/null 2>&1; then
    addgroup -g ${PGID:-1000} vger
    adduser -u ${PUID:-1000} -D -H -G vger vger   
fi

exec /bin/tcpsvd -E -u vger:vger 0.0.0.0 ${PORT:-1965} /vger $@
